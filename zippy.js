var Notesheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Notes");
var Lastrow = Notesheet.getDataRange().getLastRow();
var lastr = Lastrow + 1;

var Ticketsheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Tickets");
var TLastrow = Ticketsheet.getDataRange().getLastRow();
var Tlastr = TLastrow + 1;

var CustomersSheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Customers");
var CLastrow = CustomersSheet.getDataRange().getLastRow();
var Clastr = CLastrow + 1;

var webhookLog = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Webhook log");
var WLastrow = webhookLog.getDataRange().getLastRow();
var Wlastr = WLastrow + 1;

var NoteTickets = [];

function doPost(e) {
  
  //MailApp.sendEmail("test@gmail.com","Zippykind Webhook Data",JSON.stringify(e.postData));
  webhookLog.getRange("A"+Wlastr ).setValue(JSON.stringify(e.postData));
  Logger.log("webhook data");
  Logger.log(JSON.stringify(e.postData));
  var data = JSON.parse(e.postData.contents);
  
  if(data['hand_shake_key'] == "KLDSK8292kYsll02"){
    if(data['event_name'] == "added_note"){
      Notesheet.getRange("A"+lastr ).setValue(data['date_stamp']);
      Notesheet.getRange("B"+lastr ).setValue(data['data']['driver_id']);
      Notesheet.getRange("C"+lastr ).setValue(data['data']['ticket_id']);
      Notesheet.getRange("D"+lastr ).setValue(data['data']['note']);
      
      var res_data2 = trackticket(data['data']['ticket_id']);
      
      if(res_data2.code == 200 ){
        var customerEmail = res_data2.details.ticket_details.email_address;
        
        var customerRow = getRowByCustomer(customerEmail);
        
        if(customerRow == ''){
          var CLastrow = CustomersSheet.getDataRange().getLastRow();
          var Clastr = CLastrow + 1;
          CustomersSheet.getRange("A"+Clastr ).setValue(customerEmail);
        }
        
        var ticketRow = getRowByTicketId(data['data']['ticket_id']);
        if(ticketRow == ''){
          var TLastrow = Ticketsheet.getDataRange().getLastRow();
          var Tlastr = TLastrow + 1;
          Ticketsheet.getRange("A"+Tlastr ).setValue(data['date_stamp']);
          Ticketsheet.getRange("B"+Tlastr ).setValue(data['data']['ticket_id']);
          Ticketsheet.getRange("C"+Tlastr ).setValue("started");
          Ticketsheet.getRange("D"+Tlastr ).setValue(customerEmail);
        }
      }
    }
    if( data['event_name'] == "ticket_successful"){
      var ticketRow = getRowByTicketId(data['data']['ticket_id']);
      if(ticketRow != ''){
        var TLastrow = Ticketsheet.getDataRange().getLastRow();
        var Tlastr = TLastrow + 1;
        Ticketsheet.getRange("C"+ticketRow ).setValue(data['data']['status']);
      }
      //sendTicketInvoice(data['data']['ticket_id']);
    }  
    if(data['event_name'] == 'ticket_started'){
      
      var res_data2 = trackticket(data['data']['ticket_id']);
      
      if(res_data2.code == 200 ){
        var customerEmail = res_data2.details.ticket_details.email_address;;
        var customerRow = getRowByCustomer(customerEmail);
        if(customerRow == ''){
          var CLastrow = CustomersSheet.getDataRange().getLastRow();
          var Clastr = CLastrow + 1;
          CustomersSheet.getRange("A"+Clastr ).setValue(customerEmail);
        }
        
        var ticketRow = getRowByTicketId(data['data']['ticket_id']);
        var TLastrow = Ticketsheet.getDataRange().getLastRow();
        var Tlastr = TLastrow + 1;
        if(ticketRow == ''){
          Ticketsheet.getRange("A"+Tlastr ).setValue(data['date_stamp']);
          Ticketsheet.getRange("B"+Tlastr ).setValue(data['data']['ticket_id']);
          Ticketsheet.getRange("C"+Tlastr ).setValue(data['data']['status']);
          Ticketsheet.getRange("D"+Tlastr ).setValue(customerEmail);
        }else{
          Ticketsheet.getRange("C"+ticketRow ).setValue(data['data']['status']);
        }
      } 
    }
  } 
  
  var content = { "details" : { "zippy_token":"da2YmoRdfcK4rcvsqSGS" }}; 
  return ContentService.createTextOutput(JSON.stringify(content) ).setMimeType(ContentService.MimeType.JSON); 
}

function proccessTickets(customersEmail){  
  var startedTickets = ticketRows(customersEmail);
  Logger.log("Tickets rows for "+customersEmail+" : "+startedTickets);
  Logger.log("Total Tickets rows: "+startedTickets.length);
  if (startedTickets.length > 0){
    var emaildata = "";
    var oldDate = new Date();
    oldDate.setDate(oldDate.getDate() - 1);
    var today = new Date();
    var t = 1;
    for (var i = 0; i < startedTickets.length; i++) {
      
      var k = startedTickets[i];
      Logger.log("Row "+k+" Proccessing");
      var ticket_id = Ticketsheet.getRange("B"+k).getValue();    
      
      var result_json = tickethistory(ticket_id);
      
      if(result_json.code == 200 ){
        var notesFound = false;
        var emailnote = '';
        var receivedBy = "";
        for(var a=0;a < result_json.details.ticket_history.length; a++){
          Logger.log("ticket_history.status");
          Logger.log(result_json.details.ticket_history[a].status);
          
          var noteDate = new Date(result_json.details.ticket_history[a].date_created);
          
          Logger.log("received_by");
          Logger.log(result_json.details.ticket_history[a].received_by);
          if(result_json.details.ticket_history[a].received_by != ""){
             Logger.log("received_by found");
            receivedBy += '<b>Received by:</b> <span>'+result_json.details.ticket_history[a].received_by+'</span> <br>';
            receivedBy += '<b>Date:</b> '+result_json.details.ticket_history[a].date_created+' <br>';
          }
          if(result_json.details.ticket_history[a].status == "notes"){

            Logger.log("olddate");
            Logger.log(new Date(oldDate));
            Logger.log("noteDate");
            Logger.log(new Date(noteDate));
            
            if(new Date(oldDate).setHours(0, 0, 0, 0) == new Date(noteDate).setHours(0, 0, 0, 0)){

              Logger.log("note found" + result_json.details.ticket_history[a].notes);
              emailnote += '<b>Note:</b> <span style="color:blue;">'+result_json.details.ticket_history[a].notes+'</span> <br>';              
              emailnote +='<b>Date:</b> '+result_json.details.ticket_history[a].date_created+' <br><br>';
              notesFound = true;
            }
          }
          
        }
        Logger.log("isnotesFound");
        Logger.log(notesFound);
        
        Logger.log('email t row'+t);
        if(notesFound == true){
          Logger.log("emailnote");
          Logger.log(emailnote);
          Logger.log("receivedBy");
          Logger.log(receivedBy);
          var res_data = trackticket(ticket_id);
          if(res_data.code == 200 ){
            emaildata += '<div style="width: 700px;">';
            if(t > 1){
              emaildata +='<div style="border: 1px dashed #000; width: 100%;"></div><br>';
            }           
            emaildata += '<b>Customer Name:</b> <span style="color:red;"><b>'+res_data.details.ticket_details.customer_name+'</b></span> <br>';
            emaildata += "<b>Email:</b> "+res_data.details.ticket_details.email_address+" <br>";
            emaildata += "<b>Address:</b> "+res_data.details.ticket_details.delivery_address+" <br>";
            emaildata += "<b>Ticket Id:</b> "+res_data.details.ticket_details.ticket_id+" <br>";
            emaildata += "Transaction Type: "+res_data.details.ticket_details.trans_type+" <br>";
            emaildata += "<b>Delivery Date:</b> "+res_data.details.ticket_details.delivery_date+" <br><br>";
            emaildata += "<b>Ticket Description:</b> "+res_data.details.ticket_details.ticket_description+" <br>";           
            emaildata += '<br><span style="color:red; text-align:center;"><b style="padding-left: 170px;">LOGISTICS AGENT / COMMUNICATIONS OFFICE COMMENTS</b></span> <br><br>'+emailnote+' '+receivedBy;
            emaildata += '<br></div>';
            t++;
          }
        }
      }
    }
    
//        cc:'test@gmail.com'
    var toEmail = customersEmail;
    var blob = UrlFetchApp.fetch("https://drive.google.com/uc?export=download&id=17AeJClP6B0kOicRB6z2PEpK-2NSUZeG8").getBlob();

    if (emaildata != "") {
      Logger.log("emaildata");
      Logger.log(emaildata);
      // emaildata += '<img src="cid:image"><br/>';

      var img = UrlFetchApp.fetch("https://drive.google.com/uc?export=download&id=17AeJClP6B0kOicRB6z2PEpK-2NSUZeG8");
      var finalImage = img.getBlob().getContentType()+';base64,'+ Utilities.base64Encode(img.getBlob().getBytes());
      var attData = emaildata+'<br/><br/><br/><img src="data:'+finalImage+'"><br/>';
      var htmlData = emaildata+'<br/><img src="cid:image"><br/>';

      var fileName = Date.now();
      var fileNamePdf = fileName+".pdf";
      var fileNameHtml = fileName+".html";

      var blob1 = Utilities.newBlob(attData, "text/html", fileNameHtml);
      var pdf = blob1.getAs("application/pdf");
      

      DriveApp.createFile(pdf).setName(fileNamePdf);

       var options = {
    
        "htmlBody": htmlData,
        "inlineImages": {image: blob },
        "name":"test report",
        "cc":"test@gmail.com",
        "attachments":pdf
        
      }  

      
      GmailApp.sendEmail(toEmail, 'test reportSummary Report (LSR) for ' +Utilities.formatDate(oldDate, 'GMT-4', 'MM-dd-yyyy'), "plainbody", options);


      // MailApp.sendEmail({
      //   to: toEmail,
      //   name:'test report',
      //   subject: 'test reportSummary Report (LSR) for ' +Utilities.formatDate(oldDate, 'GMT-4', 'MM-dd-yyyy'),
      //   htmlBody: emaildata
      //   //cc:'test@gmail.com'
      // });
      Logger.log("Email sent to: "+toEmail);
      Logger.log("//////////////////////////////////////////////////////////////");
    }
  }
}

function proccessCustomers(){
  NoteTickets = getNotesTicket();
  Logger.log("Note tickets: "+NoteTickets);
  var customers = CustomersSheet.getDataRange().getValues();
  Logger.log("Total Customers rows: "+customers.length);
  if (customers.length > 0){
    for (var i = 0; i < customers.length; i++) {
      var customersEmail = customers[i][0];
      Logger.log("Customer email: "+customersEmail);
      proccessTickets(customersEmail);
    }
  }
}


function ticketRows(customersEmail){
  var range = Ticketsheet.getDataRange();
  var values = range.getValues();
  var blankRowsArray = [];
  for(var i=1;i<values.length;i++){
    if(values[i][3] == customersEmail && NoteTickets.indexOf(values[i][1]) != -1){
      blankRowsArray.push(i+1);
    }
  }
  return blankRowsArray;
}

function customerRows(){
  var data = CustomersSheet.getDataRange().getValues();
  var blankRowsArray = [];
  for(var i=1;i<data.length;i++){
    //    if(data[i][0].toString() == flag){
    blankRowsArray.push(i+1);
    //    }
  }
  return blankRowsArray;
}

function getRowByTicketId(ticket_id){
  var data = Ticketsheet.getDataRange().getValues();
  var row = '';
  for(var i=0;i<data.length;i++){
    if(data[i][1].toString() == ticket_id.toString()){
      row = i+1;
      break;
    }
  }
  return row;
}

function getRowByCustomer(email){
  var data = CustomersSheet.getDataRange().getValues();
  var row = '';
  for(var i=0;i<data.length;i++){
    if(data[i][0] == email){
      row = i+1;
      break;
    }
  }
  return row;
}

function getNotesTicket(){
  var range = Notesheet.getDataRange();
  var values = range.getValues();
  var blankRowsArray = [];
  var oldDate = new Date();
  oldDate.setDate(oldDate.getDate() - 1);
  
  for(var i=1;i<values.length;i++){
    var noteDate = new Date(values[i][0]);
    if(oldDate.getDate() == noteDate.getDate()){
      blankRowsArray.push(values[i][2]);
    }
  }
  return blankRowsArray;
}

function trackticket(ticket_id){
  
  var url = 'https://zippykind.com/api/v2/';
  var payload = {
    "scope" : "trackTicket",
    "ticket_id": ticket_id
  };
  
  var settings = { 
    "method": "POST",
    "muteHttpExceptions": true,
    "headers" : 
    {
      'apikey': 'apiKey'
    },
    "payload" : payload
  };
  
  var result = UrlFetchApp.fetch(url, settings).getContentText();
  Logger.log("trackticket result");
  Logger.log(result);
  
  return JSON.parse(result);
}

function tickethistory(ticket_id){
  var url = 'https://zippykind.com/api/v2/';
  var payload = {
    "scope" : "ticketHistory",
    "ticket_id": ticket_id
  };
  
  var settings = { 
    "method": "POST",
    "muteHttpExceptions": true,
    "headers" : 
    {
      'apikey': 'apikey'
    },
    "payload" : payload
  };
  
  var result = UrlFetchApp.fetch(url, settings).getContentText();
  Logger.log("ticketHistory result");
  Logger.log(result);
  return JSON.parse(result);
}

function sendTicketInvoice(ticketId){
//  var ticketId = '724609';
  var ticketData = trackticket(ticketId);
  if(ticketData.code == 200 ){
    var ticket = ticketData.details.ticket_details;
    var order_details = ticketData.details.order_details;
    var orderitems = ticketData.details.order_items;
    var orderitem = [];
    orderitem[0]=[];
    orderitem[0][0] = "Quantity";
    orderitem[0][1] = "Product Name";
    orderitem[0][2] = "Weight";
    orderitem[0][3] = "Sub Total";
    
    for(var i=1;i<=orderitems.length;i++){
      orderitem[i]=[];
      orderitem[i][0]=orderitems[i-1].quantity;
      orderitem[i][1]=orderitems[i-1].product_name;
      orderitem[i][2]=orderitems[i-1].weight;
      orderitem[i][3]=orderitems[i-1].sub_total;
    }
    Logger.log('orderitem');
    Logger.log(orderitem);
    
    var result_json = tickethistory(ticketId);
    var imagesingturelink = '';
    var receivedBy = "";
    if(result_json.code == 200 ){    
      for(var a=0;a < result_json.details.ticket_history.length; a++){
        Logger.log("ticket_history.status");
        Logger.log(result_json.details.ticket_history[a].status);
        
        Logger.log("received_by");
        Logger.log(result_json.details.ticket_history[a].received_by);
        if(result_json.details.ticket_history[a].received_by != ""){
          Logger.log("received_by found");
          receivedBy += result_json.details.ticket_history[a].received_by;
          receivedBy += '\n'+result_json.details.ticket_history[a].date_created;
        }
        if(result_json.details.ticket_history[a].status == "sign"){    
          imagesingturelink += result_json.details.ticket_history[a].customer_signature+'\n';              
        }
        if(result_json.details.ticket_history[a].status == "photo"){    
          imagesingturelink += result_json.details.ticket_history[a].photo_url+'\n';              
        }
        
      }
    }
    
    Logger.log("imagesingturelink");
    Logger.log(imagesingturelink);
    Logger.log("receivedBy");
    Logger.log(receivedBy);
    
    var docId = 'ghdfhdfhjdfhjdfjfghjfgj';
    
    var cp_doc = DriveApp.getFileById(docId);
    Logger.log(cp_doc.getName());
    var folder = DriveApp.getFolderById('1PpFO5jGGNT3Bart_dS6ZaaxR1BklI4tK');
    Logger.log(folder.getName());
    var newdoc = cp_doc.makeCopy(ticketId,folder);
    Logger.log(newdoc.getName());
    Logger.log(newdoc.getId());
    var doc = DocumentApp.openById(newdoc.getId());
    Logger.log(doc.getName());
    Logger.log(doc.getBody().getText());
    var body = doc.getBody();
    
    body.replaceText("{ticketid}", ticket.ticket_id);
    body.replaceText("{deliveredon}", ticket.delivery_date);
    body.replaceText("{customernumber}", ticket.ext_customer_number);
    body.replaceText("{invoicenumber}", ticket.ext_invoice_number);
    body.replaceText("{pickupfrom}",ticket.product_pickup_name+" \n"+ticket.product_pickup_contact_number+" \n"+ticket.product_pickup_address);
    body.replaceText("{deliverto}", ticket.delivery_address);
    body.replaceText("{description}", ticket.ticket_description);
    body.replaceText("{packagereceivedby}", receivedBy);
    body.replaceText("{image&singturelink}", imagesingturelink);
    body.replaceText("{promocode}", order_details.promo_code);
    body.replaceText("{taxdelivery}", order_details.tax_percent);
    body.replaceText("{delfee}", order_details.delivery_fee);
    body.replaceText("{proccessingfee}", 'proccessingfee');
    body.replaceText("{totalamount}", order_details.total);
    body.replaceText("{ticketid2}", ticket.ticket_id);
    body.replaceText("{todaysdate}", Utilities.formatDate(new Date(), 'GMT-4', 'yyyy-MM-dd'));
    
    var rgel = body.findText("{orderitems}");
    var element = rgel.getElement();
    var childIndex = body.getChildIndex(element.getParent());
    
    body.getChild(childIndex).asText().setText('');
    var odtable = body.insertTable(childIndex,orderitem);
    var cellStyle = {};
    cellStyle[DocumentApp.Attribute.FONT_SIZE] = '12';
    cellStyle[DocumentApp.Attribute.BOLD] = false;
    cellStyle[DocumentApp.Attribute.FOREGROUND_COLOR] = '#000000';
    
    odtable.setAttributes(cellStyle);
    //Save and close the document
    doc.saveAndClose();
    
    //    cc:'test@gmail.com'
    //    var toEmail = ticket.email_address;

    var toEmail = "test@gmail.com";
    var emaildata = invoice_email;
    var attachments = [doc.getBlob()];
    if (emaildata != "") {
      Logger.log("emaildata");
      Logger.log(emaildata);
      MailApp.sendEmail({
        to: toEmail,
        name:'test report',
        subject: 'test report#'+ticketId+' Ticket Invoice ',
        htmlBody: emaildata,
        cc:'test@gmail.com',
        attachments:attachments
      });
      Logger.log("Email sent to: "+toEmail);
      Logger.log("//////////////////////////////////////////////////////////////");
    }
  }
}

function emailSend(){
  
  Logger.log(Date.now());
}

function htmlToPDF() {

  var html = "<h1>Hello world</h1>"
           + "<p>The quick brown fox jumped over the lazy dog";

  var blob = Utilities.newBlob(html, "text/html", "text.html");
  var pdf = blob.getAs("application/pdf");

  DriveApp.createFile(pdf).setName("text.pdf");

  GmailApp.sendEmail("palash3593agrawal@gmail.com", "PDF File", "",
     {htmlBody: html, attachments: pdf});

}
